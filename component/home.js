import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  Button,
  TouchableOpacity,
  TextInput,
  View
} from 'react-native';

class Home extends Component {
  static navigationOptions = {
    title: 'Home', header: null
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1 , backgroundColor:'white'}}>
      <ScrollView style={{
        backgroundColor: 'white',
        flex: 1
      }}>
        <TouchableOpacity
          style={{ marginTop: -20, height: 80 }}

          onPress={() => navigate('Contact')}>
          <Image
            style={{ flex: 1, height: undefined, width: undefined }}
            resizeMode="contain"
            source={require('../resource/png/home/top_bar_home.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginTop: 15 }}

          onPress={() => navigate('Register')}>
          <Image
            resizeMode='contain'
            style={{ width: 200, height: 55, alignSelf: 'center' }}

            source={require('../resource/png/home/urgent.png')}
          />
        </TouchableOpacity>
        <View>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Name </Text>
        <TextInput maxLength={20} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Your Name'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Desease </Text>
        <TextInput maxLength={20} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='What is your ilness'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Location </Text>
        <TextInput maxLength={30} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Where is your location'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Description (optional) </Text>
        <TextInput multiline={true} maxLength={250} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Describe here'></TextInput>
        </View>
        {/* <View>
        <TouchableOpacity
        style={{ marginTop: 5 }}
          onPress={() => navigate('Contact')}>
          > */}
        <Image
            resizeMode='contain'
            style={{ width: 260, height: 70, alignSelf: 'center' }}
            source={require('../resource/png/home/request.png')}
          />
        {/* </TouchableOpacity>
        </View> */}


        
      </ScrollView>
      {/* <View style={{ backgroundColor:'white',marginBottom:0}}> */}
      <Image
          resizeMode='contain'
          style={{backgroundColor:'transparent' , width: 400, height: 70, alignSelf: 'center' , marginBottom:-5}}

          source={require('../resource/png/home/bot_bar.png')}
        />
        {/* </View> */}
      </View>
    );
  }
}

export default Home;

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20,
    backgroundColor: 'white',
    flex: 1
  }
});