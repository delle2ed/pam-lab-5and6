import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableHighlight,
  TextInput,
  ScrollView,
  TouchableOpacity
} from 'react-native';

class List extends Component {
    static navigationOptions = {
      title: 'List', header:null 
    };
    render() {
      const { navigate } = this.props.navigation;
      return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView>
          <TouchableOpacity onPress={() => navigate('Register')}>
            <Image
              style={{ width: 400, height: 60 }}
              source={require('../resource/png/doc_list/doctor_list.png')}
            />
          </TouchableOpacity>
            <Image resizeMode='contain'
              style={{ alignSelf: 'center', width: 400, height: 650 , marginTop:0}}
              source={require('../resource/png/doc_list/list.png')}
            />

        </ScrollView>
        <View>
          <Image
            resizeMode='contain'
            style={{ backgroundColor: 'transparent', width: 400, height: 70, alignSelf: 'center', marginBottom: -5 }}

            source={require('../resource/png/doc_contact/bot_bar.png')}
          />
        </View>
      </View>
      );
    }
  }

  export default List;