import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  TextInput,
} from 'react-native';

class Login extends Component {
  static navigationOptions = {
    title: 'Login', header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      password: '',
      email: ''
    };
  }

  check() {
    const { navigate } = this.props.navigation;
    if (this.state.password == '123' | this.state.email == '123') { navigate('Home') }
    else { navigate('Wrong') }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <Image
          style={styles.background}
          source={require('../resource/png/login/login.png')}>
        </Image>
        <View style={{ position: 'absolute', top: 238, alignItems: 'center' }}>
          <TextInput maxLength={20} placeholderTextColor="white" underlineColorAndroid='transparent' style={{ minWidth: 260, color: "white", fontSize: 16 }} placeholder="Email Adress" onChangeText={(email) => this.setState({ email })}
            value={this.state.email}></TextInput>
        </View>
        <View style={{ position: 'absolute', top: 326, alignItems: 'center' }}>
          <TextInput maxLength={20} secureTextEntry placeholderTextColor="white" underlineColorAndroid='transparent' style={{ minWidth: 260, color: "white", fontSize: 16 }} placeholder="Password" onChangeText={(password) => this.setState({ password })}
            value={this.state.password}></TextInput>
        </View>
        <TouchableOpacity
          style={[styles.btnLogin]}
          onPress={() => this.check()}
        >
          <Image resizeMode='contain'
            style={{ width: 300, marginTop: 0, alignSelf: 'center' }}
            source={require('../resource/png/login/btn_login.png')}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.btnSignup]}
          onPress={() => navigate('Register')}
        >
          <Image resizeMode='contain'
            style={{ width: 300, marginTop: 0, alignSelf: 'center' }}
            source={require('../resource/png/login/btn_login_2.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default Login;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#000000',
    width: 400
  },
  background: {
    paddingTop: 0,
    width: 400,
    height: 600
  },
  btnLogin: {
    width: 340,
    borderRadius: 0,
    position: 'absolute',
    left: 30,
    top: 365,
    height: 95
  },
  btnSignup: {
    width: 340,
    borderRadius: 0,
    position: 'absolute',
    left: 30,
    top: 450,
    height: 95
  },
});