import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ScrollView,
  TextInput
} from 'react-native';

class Register extends Component {
  static navigationOptions = {
    title: 'Register', header: null
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView style={{
        backgroundColor: 'white',
        flex: 1
      }}>
        <View>
          <TouchableOpacity
            onPress={() => navigate('Login')}
          >
            <Image resizeMode='cover'
              style={{ width: 400, height: 50 }}
              source={require('../resource/png/register/register.png')}
            />
          </TouchableOpacity>
        </View>
        <View style={{ alignSelf: 'center' }}>
          <Image resizeMode='center'
            style={{ marginTop: 15, width: 125, height: 125 }}
            source={require('../resource/png/register/photo.png')}
          />
        </View>

      <View>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Full Name </Text>
        <TextInput maxLength={30} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Your Full Name'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Birth Date </Text>
        <TextInput maxLength={10} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='dd/mm/yy'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Email</Text>
        <TextInput maxLength={30} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Your Email'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Phone Number </Text>
        <TextInput maxLength={13} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Your Phone Number'></TextInput>
        <Text style={{ marginTop: 10, marginLeft: 40 }}>Location/Adress </Text>
        <TextInput maxLength={30} style={{ marginTop: 6, marginLeft: 50, maxWidth: 260, minWidth: 260, color: "gray", fontSize: 12 }} placeholderTextColor="gray" underlineColorAndroid='transparent' placeholder='Your Location'></TextInput>

      </View>


      <View style={{alignSelf:'center'}}>
      <TouchableOpacity
          style={{ marginTop: 15 , marginBottom:15}}

          onPress={() => navigate('Notification')}>
        <Image resizeMode='cover'
              style={{ width: 300, height: 50 }}
              source={require('../resource/png/register/next.png')}
            />
            </TouchableOpacity>
          </View>

      </ScrollView>
    );
  }
}

export default Register;