import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  ScrollView
} from 'react-native';

class Contact extends Component {
  static navigationOptions = {
    title: 'Contact', header: null
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <ScrollView>
          <TouchableOpacity onPress={() => navigate('List')}>
            <Image
              style={{ width: 400, height: 50 }}
              source={require('../resource/png/doc_contact/details.png')}
            />
          </TouchableOpacity>
          <View>
            <Image resizeMode='contain'
              style={{ alignSelf: 'center', width: 300, height: 500 }}
              source={require('../resource/png/doc_contact/contact.png')}
            />
            <Image resizeMode='contain'
              style={{ alignSelf: 'center', width: 300, height: 50 , marginTop:10 }}
              source={require('../resource/png/doc_contact/request.png')}
            />
          </View>

        </ScrollView>
        <View>
          <Image
            resizeMode='contain'
            style={{ backgroundColor: 'transparent', width: 400, height: 70, alignSelf: 'center', marginBottom: -5 }}

            source={require('../resource/png/doc_contact/bot_bar.png')}
          />
        </View>
      </View>
    );
  }
}

export default Contact;