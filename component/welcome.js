import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableHighlight,
  TouchableOpacity
} from 'react-native';

import { NavigationActions } from 'react-navigation'

class Welcome extends Component {


  static navigationOptions = {
    title: 'Welcome', header: null
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View >
        <Image
          resizeMode='cover'
          style={{ width: 400, height: 600 }}
          source={require('../resource/welcome_screen.png')}
        />
        <TouchableOpacity
          style={styles.btnSignup}
          onPress={() => navigate('Register')}
        >
          <Text style={styles.btnSignupText}>Sign Up</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.btnLogin]}
          onPress={() => navigate('Login')}
        >
          <Image     resizeMode='contain'
            style={{ width:300, marginTop: -25, alignSelf:'center'}}
            source={require('../resource/welcome_screen_btn2.png')}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default Welcome;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnSignup: {
    backgroundColor: 'white',
    width: 280,
    height: 50,
    borderRadius: 50,
    position: 'absolute',
    left: 60,
    bottom: 200,
  },
  btnLogin: {
    width: 340,
    height: 100,
    borderRadius: 5,
    position: 'absolute',
    left: 30,
    bottom: 110,
  },
  btnSignupText: {
    color: '#08DA5F',
    padding: 13,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btnLoginText: {
    color: 'white',
    padding: 20,
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
