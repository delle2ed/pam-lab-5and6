import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableHighlight
} from 'react-native';

import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
  index: 0,
  actions: [
    NavigationActions.navigate({ routeName: 'Welcome'})
  ]
})

class Splash extends Component {
    static navigationOptions = {
      title: 'Splash', header:null 
    };
    render() {
      return (
        <View>
            <TouchableHighlight  onLayout={() => setTimeout(()=>{ this.props.navigation.dispatch(resetAction)
},1300)}>
      <Image resizeMode='cover'
         style={{width: 400, height: 600}}
        source={require('../resource/splash.png')}
      />
    </TouchableHighlight>     
        </View>
      );
    }
  }

  export default Splash;
