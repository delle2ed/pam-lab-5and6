import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableHighlight,
  ScrollView
} from 'react-native';

class Notification extends Component {
  static navigationOptions = {
    title: 'Notification', header: null
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1 , backgroundColor:'white'}}>
        <ScrollView
          style={{
            backgroundColor: 'white',
            flex: 1
          }}>
          <TouchableHighlight
            onPress={() => navigate('Home')}
          >
            <Image
              style={{ width: 400, height: 50 }}
              source={require('../resource/png/aprove_notif/notif.png')}
            />
          </TouchableHighlight>
          <Image
            style={{ width: 220, height: 260, alignSelf: 'center', marginTop: 15 }}
            source={require('../resource/png/aprove_notif/aproved.png')}
          />
          <View>
            <Text style={{ color: 'black', marginTop: 10, marginLeft: 40 }}>Name </Text>
            <Text style={{ marginTop: 5, marginLeft: 40 }}>Jojon Suehndra </Text>
            <Text style={{ color: 'black', marginTop: 10, marginLeft: 40 }}>Desease</Text>
            <Text style={{ marginTop: 5, marginLeft: 40 }}>Sore Eyes </Text>
            <Text style={{ color: 'black', marginTop: 10, marginLeft: 40 }}>Location</Text>
            <Text style={{ marginTop: 5, marginLeft: 40 }}>St Broxlyn 212 </Text>
            <Text style={{ color: 'black', marginTop: 10, marginLeft: 40 }}>Description</Text>
            <Text style={{ marginTop: 5, marginLeft: 40 }}>Aku ingin menanju settiik awan kecil di langint . </Text>
          </View>
          <Image
            style={{ width: 310, height: 140, alignSelf: 'center', marginTop: 30 }}
            source={require('../resource/png/aprove_notif/doc.png')}
          />
          <Image
            style={{ width: 280, height: 45, alignSelf: 'center', marginTop: 25 }}
            source={require('../resource/png/aprove_notif/confirm.png')}
          />
          <Image
            style={{ width: 280, height: 48, alignSelf: 'center', marginTop: 15, marginBottom: 5 }}
            source={require('../resource/png/aprove_notif/cancel.png')}
          />
        </ScrollView>
        <View>
          <Image
            style={{ width: 380, height: 55, alignSelf: 'center', marginTop: 15, marginBottom: 0 }}
            source={require('../resource/png/aprove_notif/bar.png')}
          />
        </View>
      </View>
    );
  }
}

export default Notification;