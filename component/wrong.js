import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    Button,
    TouchableHighlight
} from 'react-native';

import { NavigationActions } from 'react-navigation'

const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
        NavigationActions.navigate({ routeName: 'Login' })
    ]
})

class Wrong extends Component {
    static navigationOptions = {
        title: 'Wrong', header: null
    };
    render() {
        return (
            <View>
                <TouchableHighlight onLayout={() => setTimeout(() => {
                    this.props.navigation.dispatch(resetAction)
                }, 400)}>
                    <Image
                        style={{ width: 400, height: 600 }}
                        source={require('../resource/background.jpg')}
                    />
                </TouchableHighlight>
                <View style={{ position: 'absolute', top: 250, left:80 , alignItems: 'center' }}>
                    <Text  underlineColorAndroid='transparent' style={{alignItems: 'center', color: "white", fontSize: 30 , fontFamily:'Corporative Sans Rounded Bold' }}>Wrong credentials</Text>
                </View>
            </View>
        );
    }
}

export default Wrong;