import { StackNavigator } from 'react-navigation';

import Splash from './component/splash.js';
import Welcome from './component/welcome.js';
import Register from './component/register.js';
import Login from './component/login.js';
import Wrong from './component/wrong.js'
import Home from './component/home.js'
import Notification from './component/aprove_notif.js'
import Contact from './component/doctor_contact.js'
import List from './component/doctor_list.js'

export const AppNav = StackNavigator({
    Splash: { screen: Splash },
    Welcome: { screen: Welcome},
    Register: {screen: Register},
    Login : {screen: Login} ,
    Wrong : {screen: Wrong} ,
    Home  : {screen: Home},
    Notification : {screen: Notification},
    Contact : {screen: Contact} ,
    List :{screen: List}  },

  );

export default AppNav;