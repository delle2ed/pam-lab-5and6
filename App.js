import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button
} from 'react-native';

import { StackNavigator } from 'react-navigation';

import AppNav from './appNav.js'


export default class App extends Component<{}> {
  render() {
    return (
      <AppNav></AppNav>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});
